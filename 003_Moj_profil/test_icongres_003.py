# -*- coding: utf-8 -*-
import unittest
from selenium import webdriver
import page
import time
import os
from faker import Faker

class Icongres(unittest.TestCase):
    
    def setUp(self):
        self.driver = webdriver.Chrome()
        self.driver.get("https://icongress.prod01.stermedia.eu/")

    def test_data_update(self):
       
        main_page = page.MainPage(self.driver)

        main_page.click_on_element(page.MainPageElements.COOKIE_MONSTER) 
    
        main_page.click_on_element(page.MainPageElements.LOGIN_TAB)
               
        main_page.login(os.environ['MY_EMAIL'], os.environ['MY_PASS'])


        main_page.click_on_element(page.MainPageElements.PROFILE_TAB)
        
        main_page.fake_data()
        
        main_page.insert_old_password = os.environ['MY_PASS']
        main_page.insert_street = main_page.fake_pl.street_name()
        main_page.insert_street_number = main_page.fake_pl.building_number()
                
                
        main_page.scroll_and_click_on_element(page.MainPageElements.COOKIE_MONSTER2)

        main_page.scroll_and_click_on_element(page.MainPageElements.SAVE_CHANGES)
        
        
        search_results_page = page.SearchResultsPage(self.driver)
        assert search_results_page.is_results_found(), "Błąd! Dane nie zostały zapisane"
        

    def tearDown(self):
        self.driver.close()

if __name__ == "__main__":
    unittest.main()