# -*- coding: utf-8 -*-
import unittest
from selenium import webdriver
import page
import time
import os
import random

class Icongres(unittest.TestCase):
    
    def setUp(self):
        self.driver = webdriver.Chrome()
        self.driver.get("https://icongress.prod01.stermedia.eu/")
        main_page = page.MainPage(self.driver)
        main_page.create_file(page.TemporaryFile.TEMPORARY_FILE)

    def test_data_update(self):

        
        main_page = page.MainPage(self.driver)

        main_page.click_on_element(page.MainPageElements.COOKIE_MONSTER) 
    
        main_page.click_on_element(page.MainPageElements.LOGIN_TAB)
               
        main_page.login(os.environ['MY_EMAIL'], os.environ['MY_PASS'])


        main_page.click_on_element(page.MainPageElements.ABSTRACT_TAB)

        main_page.scroll_and_click_on_element(page.MainPageElements.COOKIE_MONSTER2) 
        
        main_page.scroll_and_click_on_element(page.MainPageElements.REPORT_NEW_ABSTRACT)

        main_page.select(page.MainPageElements.CONFERENCE, random.choice(page.NumberList.CONFERENCE_NUMBER_LIST))

        main_page.fake_data()

        main_page.insert_title_pl = main_page.fake_pl.catch_phrase()
        main_page.insert_title_eng = main_page.fake_eng.catch_phrase()
        main_page.insert_afiliation = main_page.fake_pl.company()     
        
        main_page.add_document = page.TemporaryFile.FILE_PATH        
        
        time.sleep(1)
        
        main_page.scroll_and_click_on_element(page.MainPageElements.REPORT_ABSTRACT)   
        
        search_results_page = page.SearchResultsPage(self.driver)
        assert search_results_page.is_results_found(), "Niepoprawny format dodanego pliku"
        

    def tearDown(self):
        os.remove(page.TemporaryFile.TEMPORARY_FILE)
        print("Plik tymczasowy zostal usuniety!")
        self.driver.close()

if __name__ == "__main__":
    unittest.main()