# -*- coding: utf-8 -*-
from element import BasePageElement
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import Select
import os
from faker import Faker

class InsertLoginEmail(BasePageElement):

    locator = "//input[@placeholder='Podaj adres e-mail']"

class InsertLoginPassword(BasePageElement):

    locator = "//input[@placeholder='podaj hasło'][@id='LoginForm_password'][@type='password']"

class InsertTitlePL(BasePageElement):

    locator = "//input[@placeholder='podaj tytuł abstraktu']"

class InsertTitleENG(BasePageElement):

    locator = "//input[@placeholder='podaj angielski tytuł abstraktu']"

class InsertAfiliation(BasePageElement):

    locator = "//input[@placeholder='podaj afiliację']"

class AddDocument(BasePageElement):

    locator = "//input[@name='SynopsisApp[file]'][@type='file']"

class BasePage(object):

    def __init__(self, driver):
        self.driver = driver


class MainPageElements(object):
    
    COOKIE_MONSTER = "//button[@ class='btn btn-primary CookieMonsterOk2']"
    LOGIN_BUTTON = "//input[@class='btn btn-primary'][@type='submit']"
    LOGIN_TAB = "//ul[@class='nav navbar-nav']/li[3]"
    ABSTRACT_TAB = "//ul[@class='nav navbar-nav']/li[3]"
    REPORT_NEW_ABSTRACT = "//a[@href='/site/addabstract']"
    CONFERENCE = "//select[@class='form-control'][@name='SynopsisApp[conference_id]']"
    CHOOSE_FILE = "//input[@name='SynopsisApp[file]'][@type='file']"
    REPORT_ABSTRACT = "//input[@class='btn btn-primary'][@type='submit']"
    COOKIE_MONSTER2 = "//button[@ class='btn btn-xs btn-primary CookieMonsterOk']"
    
class TemporaryFile(object):
    
    TEMPORARY_FILE = 'Plik_tymczasowy.py'

    FILE_PATH = os.path.abspath(TEMPORARY_FILE) 

class NumberList(object):

    CONFERENCE_NUMBER_LIST = ['57','77','65','69','74']

class MainPage(BasePage):
    
    insert_login_email = InsertLoginEmail()
    insert_login_password = InsertLoginPassword()
    insert_title_pl = InsertTitlePL()
    insert_title_eng = InsertTitleENG()
    insert_afiliation = InsertAfiliation()
    add_document = AddDocument()
    
    def click_on_element(self,xpath):
        element = self.driver.find_element_by_xpath(xpath)
        element.click()

    def login(self, login, password):
        self.insert_login_email = login
        self.insert_login_password = password

        self.click_on_element(MainPageElements.LOGIN_BUTTON)

    def scroll_and_click_on_element(self,xpath):
        element = self.driver.find_element_by_xpath(xpath)
        actions = ActionChains(self.driver)
        actions.move_to_element(element)
        actions.perform()
        element.click()

    def select(self, xpath, value):
        select = Select(self.driver.find_element_by_xpath(xpath))
        select.select_by_value(value)

    def create_file(self,name):
        f = open(name,'w+')
        f.write('superciekawy tekst w srodku')
        f.close()

    def fake_data(self):
        self.fake_pl = Faker('pl_PL')
        self.fake_pl.seed(4321)
        
        self.fake_eng = Faker('en_GB')
        self.fake_eng.seed(4321)
        
class SearchResultsPage(BasePage):

    def is_results_found(self):
        return "nie może być załadowany" not in self.driver.page_source  #<-jak to to takto?
