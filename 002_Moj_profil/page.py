# -*- coding: utf-8 -*-
from element import BasePageElement
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import Select
from faker import Faker

class InsertLoginEmail(BasePageElement):

    locator = "//input[@placeholder='Podaj adres e-mail']"

class InsertLoginPassword(BasePageElement):

    locator = "//input[@placeholder='podaj hasło'][@id='LoginForm_password'][@type='password']"

class InsertOldPassword(BasePageElement):

    locator = "//input[@placeholder='podaj hasło']"

class InsertNIP(BasePageElement):

    locator = "//input[@class='form-control'][@name='User[nip]']"


class BasePage(object):

    def __init__(self, driver):
        self.driver = driver


class MainPageElements(object):
    
    COOKIE_MONSTER = "//button[@ class='btn btn-primary CookieMonsterOk2']"
    LOGIN_BUTTON = "//input[@class='btn btn-primary'][@type='submit']"
    LOGIN_TAB = "//ul[@class='nav navbar-nav']/li[3]"
    PROFILE_TAB = "//ul[@class='nav navbar-nav']/li[2]"
    PROVINCE = "//select[@class='form-control'][@name='User[billCounty]']"
    COOKIE_MONSTER2 = "//button[@class='btn btn-xs btn-primary CookieMonsterOk']"
    SAVE_CHANGES = "//input[@class='form-control btn btn-primary btn-block'][@type='submit'][@value='Zapisz zmiany']"
    COUNTRY = "//select[@class='form-control'][@name='User[billCountry]']"

class MainPage(BasePage):
    
    insert_login_email = InsertLoginEmail()
    insert_login_password = InsertLoginPassword()
    insert_old_password = InsertOldPassword()
    insert_NIP = InsertNIP()
    
    
    def click_on_element(self,xpath):
        element = self.driver.find_element_by_xpath(xpath)
        element.click()

    def login(self, login, password):
        self.insert_login_email = login
        self.insert_login_password = password

        self.click_on_element(MainPageElements.LOGIN_BUTTON)

    def scroll_and_click_on_element(self,xpath):
        element = self.driver.find_element_by_xpath(xpath)
        actions = ActionChains(self.driver)
        actions.move_to_element(element)
        actions.perform()
        element.click()

    def select(self, xpath, value):
        select = Select(self.driver.find_element_by_xpath(xpath))
        select.select_by_value(value)

    def fake_data(self):
        self.fake_pl = Faker('pl_PL')
        self.fake_pl.seed(4321)



class SearchResultsPage(BasePage):

    def is_results_found(self):
        return "uaktualnione" in self.driver.page_source  